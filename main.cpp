#include "FileLines.h"
#include <cstdlib>
#include <cassert>
#include <cstring>

void printLines( const string* source, int nLines );
void printLines( const string* source, int nLines, FILE* file );

int parseArgs( int argc, const char* argv[], char** inputFile, bool* reverse, bool* writeToFile, char** outputFile );
const int PARSE_ERROR            = -2;
const int PARSE_UNKNOWN_COMMANDS = -1;
const int PARSE_NO_ARGS          =  0;
const int PARSE_SUCCESSFUL       =  1;

int main( int argc, const char* argv[] )
{
    printf( "# LineSorter by imcub v2.3 2017\n\n" );

    bool  reverse     = false;
    char* inputFile   = nullptr;
    bool  writeToFile = false;
    char* outputFile  = nullptr;

    int parsingResult = parseArgs( argc, argv, &inputFile, &reverse, &writeToFile, &outputFile );

    switch( parsingResult )
    {
        case PARSE_ERROR:
        {
            printf( "# Argument error \n" );
            break;
        }
        case PARSE_UNKNOWN_COMMANDS:
        {
            printf( "# Unknown commands\n" );
            break;
        }
        case PARSE_NO_ARGS:
        {
            printf( "# Input \n"
                    "# lines -f [ %%filename%% ] \" \n"
                    "# -o [ %%outfile%% ] \n"
                    "# -r ( reverse sort ) \n" );
            break;
        }
        case PARSE_SUCCESSFUL:
        {
            FILE* fin = fopen( inputFile, "r" );

            if( nullptr == fin || ferror( fin ) != 0 )
            {
                printf( "Error opening file %s\n", inputFile );
                return 1;
            }

            char* buf = nullptr;
            int nLines = 0;
            string* lines = readTextFromFile( fin, &buf, &nLines );

            fclose( fin );

            qsort( lines, (size_t)nLines, sizeof( string ), reverse? stringcmp_r : stringcmp );

            if( writeToFile )
            {
                FILE* fout = fopen( outputFile, "w" );

                if( nullptr == fout || ferror( fout ) != 0 )
                {
                    printf( "Error opening file %s\n", outputFile );
                    return 1;
                }

                printLines( lines, nLines, fout );

                fclose( fout );

                printf( "Written to file %s\n" , outputFile );
            }
            else printLines( lines, nLines );

            delete[] buf;   buf   = nullptr;
            delete[] lines; lines = nullptr;

            return 0;
        }
        default:
            return -1;
    }

    return 0;
}

void printLines( const string* source, int nLines )
{
    assert( nullptr != source );
    
    for( int i = 0; i < nLines; ++i )
    {
        printf( "%s\n", source[i].str );
    }
}

void printLines( const string* source, int nLines, FILE* file )
{
    assert( nullptr != source );
    assert( nullptr != file );
    assert( ferror( file ) == 0 );

    for( int i = 0; i < nLines; ++i )
    {
        fprintf( file,  "%s\n", source[i].str );
    }
}

int parseArgs( int argc, const char* argv[], char** inputFile, bool* reverse, bool* writeToFile, char** outputFile )
{
    assert( nullptr != outputFile );
    assert( nullptr != inputFile );
    assert( nullptr != reverse );

    *outputFile = nullptr;
    *inputFile = nullptr;
    *reverse = false;
    *writeToFile = false;

    if( argc > 1 )
    {
        for( int i = 1; i < argc; ++i )
        {
            if( argv[i][0] == '-' )
            {
                int len = strlen( argv[i] );
                for( int c = 1; c < len; ++c )
                {
                    if( argv[i][c] == 'r' ) *reverse = true;
                    else if( argv[i][c] == 'f' )
                    {
                        if( argc > i + 1 && argv[i+1][0] != '-' ) *inputFile = (char* const)(argv[i + 1]);
                        else return PARSE_ERROR;
                    }
                    else if( argv[i][c] == 'o' )
                    {
                        if( argc > i + 1 && argv[i+1][0] != '-' )
                            *outputFile = (char* const)(argv[i + 1]), *writeToFile = true;
                        else return PARSE_ERROR;
                    }
                    else return PARSE_UNKNOWN_COMMANDS;
                }
            }
        }
        return PARSE_SUCCESSFUL;
    }
    else return PARSE_NO_ARGS;
}
