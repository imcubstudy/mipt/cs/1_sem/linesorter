#include <cstring>
#include <cassert>
#include <cctype>
#include <new>
#include "FileLines.h"

string::string() : str( nullptr ), length ( 0 ) {}

string::string( char* str, long len ) : str( str ), length ( len )
{
    assert( str != nullptr );
}

string::string( char* str ) : str( str )
{
    assert( str != nullptr );
    length = strlen( str );
}

string* readTextFromFile( FILE* file, char** buffer, int* numberOfLines )
{
    assert( ferror( file ) == 0 );
    assert( nullptr != file );
    assert( nullptr != buffer );
    assert( nullptr != numberOfLines );
    
    long bufSize = fileSize( file );
    *buffer = new (std::nothrow) char[bufSize + 1];
    assert( nullptr != *buffer );
    (*buffer)[bufSize] = '\0';

    bool bn = false;
    fread( *buffer, sizeof( char ), (size_t)bufSize, file );
    if( (*buffer)[bufSize - 1] != '\n' ) (*buffer)[bufSize] = '\n', bn = true;

    // Counting lines and replacing chars at the same time
    int nLines = replace( *buffer, *buffer + bufSize + ( bn? 1 : 0 ), '\n', '\0' );
    
    string* lines = new (std::nothrow) string[nLines];
    assert( nullptr != lines );
    
    char* source = *buffer;
    for( int i = 0; i < nLines; ++i )
    {
        lines[i] = string( source, strlen( source ) );
        source += lines[i].length + 1;
    }
    
    *numberOfLines = nLines;
    return lines;
}

int replace( char* begin, char* end, char oldCh, char newCh )
{
    assert( begin <= end );
    assert(nullptr != begin );
    assert(nullptr != end );

    int replaced = 0;
    
    for( char* ptr = begin; ptr < end; ptr++ )
    {
        if( *ptr == oldCh )
        {
            replaced++;
            *ptr = newCh;
        }
    }
    
    return replaced;
}

long fileSize( FILE* file )
{
    assert( nullptr != file );
    assert( ferror( file ) == 0 );
    
    long currentPosition = ftell( file );
    
    fseek( file, 0, SEEK_END );
    long size  = ftell( file );
    
    fseek( file, currentPosition, SEEK_SET );
    
    return size;
}

int stringcmp( const void* first, const void* second )
{
    assert( first  != nullptr );
    assert( second != nullptr );

    string* f_string = (string*)first;
    string* s_string = (string*)second;

    char* f_begin = f_string->str;
    char* s_begin = s_string->str;
    char* f_end = f_begin + f_string->length;
    char* s_end = s_begin + s_string->length;

    char* f_ptr = f_begin;
    char* s_ptr = s_begin;

    while( isspace( *f_ptr ) || ispunct( *f_ptr ) ) f_ptr++;
    while( isspace( *s_ptr ) || ispunct( *s_ptr ) ) s_ptr++;

    while( f_ptr < f_end &&
           s_ptr < s_end )
    {
        while( ispunct( *f_ptr ) ) f_ptr++;
        while( ispunct( *s_ptr ) ) s_ptr++;

        if( *f_ptr != *s_ptr ) return *f_ptr - *s_ptr;

        f_ptr++;
        s_ptr++;
    }

    return 0;
}

int stringcmp_r( const void* first, const void* second )
{
    assert( nullptr != first );
    assert( nullptr != second );
    
    string* f_string = (string*)first;
    string* s_string = (string*)second;

    char* f_begin = f_string->str;
    char* s_begin = s_string->str;
    char* f_end = f_begin + f_string->length;
    char* s_end = s_begin + s_string->length;

    char* f_ptr = f_end - 1;
    char* s_ptr = s_end - 1;

    while( isspace( *f_ptr ) || ispunct( *f_ptr ) ) f_ptr--;
    while( isspace( *s_ptr ) || ispunct( *s_ptr ) ) s_ptr--;

    while( f_ptr >= f_begin &&
           s_ptr >= s_begin )
    {
        while( ispunct( *f_ptr ) ) f_ptr--;
        while( ispunct( *s_ptr ) ) s_ptr--;

        if( *f_ptr != *s_ptr ) return *f_ptr - *s_ptr;

        f_ptr--;
        s_ptr--;
    }

    return 0;
}
