#include <cstdio>

struct string
{
    char* str;
    long length;
    string();
    string( char* str, long len );
    string( char* str );
};

string* readTextFromFile( FILE* file, char** buffer, int* numberOfLines );
long fileSize( FILE* file );
int replace( char* begin, char* end, char oldCh, char newCh );
int stringcmp  ( const void* first, const void* second );
int stringcmp_r( const void* first, const void* second );
